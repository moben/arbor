# Copyright 2007 Bryan Østergaard
# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="Rsync provides fast incremental file transfers"
HOMEPAGE="https://rsync.samba.org"
DOWNLOADS="${HOMEPAGE}/ftp/${PN}/src/${PNV}.tar.gz"

UPSTREAM_CHANGELOG="${DOWNLOADS/.tar.gz/-NEWS}"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~x86"
MYOPTIONS="
    acl
    xattr
    parts: binaries documentation
"

# test failures, last checked: 3.1.3
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/perl:*
    build+run:
        dev-libs/popt
        sys-libs/zlib
        acl? ( sys-apps/acl )
        xattr? ( sys-apps/attr )
"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'acl acl-support'
    'xattr xattr-support'
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-ipv6
    --with-included-zlib=no
    --without-included-popt
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( OLDNEWS )

src_install() {
    default

    install_systemd_files

    expart binaries /usr/$(exhost --target)/bin
    expart documentation /usr/share/{doc,man}
}

