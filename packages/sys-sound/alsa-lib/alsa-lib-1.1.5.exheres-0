# Copyright 2008 Alexander Færøy <eroyf@exherbo.org>
# Copyright 2009 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2010 Adriaan Leijnse <adriaan@leijnse.net>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="ALSA library"
HOMEPAGE="https://www.alsa-project.org"
DOWNLOADS="mirror://alsaproject/${PN#alsa-}/${PNV}.tar.bz2"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    doc
    python
"

DEPENDENCIES="
    build:
        doc? ( app-doc/doxygen[>=1.2.6] )
    build+run:
        python? ( dev-lang/python:2.7 )
"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( MEMORY-LEAK )

src_configure() {
    local myconf=(
        --enable-thread-safety
        --with-pythonlibs="$(/usr/$(exhost --target)/bin/python2.7-config --libs)"
        --with-pythonincludes="$(/usr/$(exhost --target)/bin/python2.7-config --includes)"
        --without-wordexp
        $(option_enable python)
    )

    econf "${myconf[@]}"
}

src_compile() {
    default

    if option doc; then
        emake doc
        edo find doc/doxygen/html -type f -print0 | xargs -0 sed -i -e "s:${WORK}::g" ||
            die "sed to strip ${WORK} from api docs failed"
    fi
}

src_install() {
    default

    # Fix alsaucm: unable to obtain card list: No such file or directory
    keepdir /usr/share/alsa/ucm

    if option doc; then
        docinto html
        dodoc -r doc/doxygen/html/*
        docinto
    fi
}

