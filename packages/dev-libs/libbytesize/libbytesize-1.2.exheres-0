# Copyright 2017 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=storaged-project release=${PV} suffix=tar.gz ] \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="Library for working with sizes in bytes"
DESCRIPTION="
The libbytesize is a C library that facilitates work with sizes in bytes. Be it parsing the input
from users or producing a nice human readable representation of a size in bytes this library takes
localization into account. It also provides support for sizes bigger than MAXUINT64.
"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="gtk-doc"

# tests run python scripts
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config[>=0.9.0]
        gtk-doc? ( dev-doc/gtk-doc )
    build+run:
        dev-libs/gmp:=
        dev-libs/mpfr:=
        dev-libs/pcre[>=8.32]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --without-python3
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    gtk-doc
)

src_prepare() {
    edo sed \
        -e '/python/d' \
        -i src/Makefile.am

    autotools_src_prepare
}

