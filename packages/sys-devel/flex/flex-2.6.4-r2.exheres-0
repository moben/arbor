# Copyright 2007 Bryan Østergaard
# Distributed under the terms of the GNU General Public License v2

require github [ user=westes release=v${PV} suffix="tar.gz" ]

SUMMARY="Tool for generating lexical scanners"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~x86"
MYOPTIONS="
    ( parts: binaries data development documentation libraries )
    ( linguas: ca da de eo es fi fr ga hr ko nl pl pt_BR ro ru sr sv tr vi zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/bison[>=2.6]
        sys-devel/gettext[>=0.18]
    build+run:
        sys-devel/m4[>=1.4]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    CC_FOR_BUILD=$(exhost --build)-cc
    --enable-nls
    --disable-static
    # Fix segfaults with glibc 2.26 (e.g. when trying to build doxygen)
    # https://github.com/westes/flex/issues/241
    CFLAGS="${CFLAGS} -D_GNU_SOURCE"
)

# -j1 for "main.c:39:19: fatal error: parse.h: No such file or directory"
DEFAULT_SRC_COMPILE_PARAMS=( -j1 AR="${AR}" )
DEFAULT_SRC_TEST_PARAMS=( -j1 )

src_test() {
    # make check wants to rebuild this just because of autoreconf
    edo touch doc/flex.pdf

    default
}

src_install() {
    # make install wants to rebuild this just because of autoreconf if it hasn't
    # been touched before. And no, doing it in src_prepare doesn't work.
    expecting_tests || edo touch doc/flex.pdf

    default

    expart binaries /usr/$(exhost --target)/bin
    expart data /usr/share
    expart development /usr/$(exhost --target)/include
    expart documentation /usr/share/{doc,info,man}
    expart libraries /usr/$(exhost --target)/lib
}

