[binaries]
c = 'i686-pc-linux-musl-cc'
cpp = 'i686-pc-linux-musl-c++'
ar = 'i686-pc-linux-musl-ar'
strip = 'i686-pc-linux-musl-strip'
pkgconfig = 'i686-pc-linux-musl-pkg-config'

[host_machine]
system = 'linux'
cpu_family = 'x86'
cpu = 'i686'
endian = 'little'
